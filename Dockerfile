# Установка базового образа Python
FROM python:3.9

# Установка переменной окружения для работы внутри контейнера
ENV PYTHONUNBUFFERED=1

# Создание рабочей директории в контейнере
WORKDIR /app

# Копирование зависимостей в контейнер
COPY requirements.txt .

# Установка зависимостей
RUN pip install -r requirements.txt

# Копирование кода приложения в контейнер
COPY . .

# Запуск команды для запуска сервера Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
