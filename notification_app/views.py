from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.decorators import action, api_view
from rest_framework.response import Response
from .models import Client, Dispatch, Message
from .serializers import ClientSerializer, DispatchSerializer, MessageSerializer
from datetime import datetime
from django.db.models import Q, Count, Max
from . import external_api
from .api_docs import openapi_dict
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class DispatchViewSet(viewsets.ModelViewSet):
    queryset = Dispatch.objects.all()
    serializer_class = DispatchSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

@swagger_auto_schema(methods=['GET'])
@api_view(['GET'])
def openapi_view(request):
    return Response(openapi_dict)

@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'phone': openapi.Schema(type=openapi.TYPE_INTEGER),
        'text': openapi.Schema(type=openapi.TYPE_STRING),
    }
))
@api_view(['POST'])
def create_client(request):
    """
    Create a new client with all attributes.
    """
    serializer = ClientSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

@swagger_auto_schema(method='put', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'phone': openapi.Schema(type=openapi.TYPE_INTEGER),
        'text': openapi.Schema(type=openapi.TYPE_STRING),
    }
))
@api_view(['PUT'])
def update_client(request, client_id):
    try:
        client = Client.objects.get(id=client_id)
        serializer = ClientSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Client.DoesNotExist:
        return Response({"error": "Client does not exist"}, status=status.HTTP_404_NOT_FOUND)

@swagger_auto_schema(method='delete')
@api_view(['DELETE'])
def delete_client(request, client_id):
    """
    Delete a client from the directory.
    """
    try:
        client = Client.objects.get(client_id=client_id)
    except Client.DoesNotExist:
        return Response(status=404)

    client.delete()
    return Response(status=204)

@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'start_time': openapi.Schema(type=openapi.TYPE_STRING),
        'end_time': openapi.Schema(type=openapi.TYPE_STRING),
        'message_text': openapi.Schema(type=openapi.TYPE_STRING),
        'client_filter': openapi.Schema(type=openapi.TYPE_OBJECT),
    }
))
@api_view(['POST'])
def create_dispatch(request):
    serializer = DispatchSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@swagger_auto_schema(methods=['GET'])
@api_view(['GET'])
def get_statistics(request):
    statistics = {
        'total_clients': Client.objects.count(),
        'total_dispatches': Dispatch.objects.count(),
        'total_messages': Message.objects.count(),
        'status_counts': Message.objects.values('status').annotate(count=Count('status')).order_by('status')
    }
    return Response(statistics, status=status.HTTP_200_OK)

@swagger_auto_schema(methods=['GET'])
@api_view(['GET'])
def get_dispatch_statistics(request, dispatch_id):
    try:
        dispatch = Dispatch.objects.get(pk=dispatch_id)
        messages = Message.objects.filter(dispatch_id=dispatch.pk)
        statistics = {
            'total_messages': messages.count(),
            'status_counts': messages.values('status').annotate(count=Count('status')).order_by('status')
        }
        return Response(statistics, status=status.HTTP_200_OK)
    except Dispatch.DoesNotExist:
        return Response({"error": "Dispatch does not exist"}, status=status.HTTP_404_NOT_FOUND)

@swagger_auto_schema(method='put', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'start_time': openapi.Schema(type=openapi.TYPE_STRING),
        'end_time': openapi.Schema(type=openapi.TYPE_STRING),
        'message_text': openapi.Schema(type=openapi.TYPE_STRING),
        'client_filter': openapi.Schema(type=openapi.TYPE_OBJECT),
    }
))
@api_view(['PUT'])
def update_dispatch(request, dispatch_id):
    try:
        dispatch = Dispatch.objects.get(pk=dispatch_id)
        serializer = DispatchSerializer(dispatch, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Dispatch.DoesNotExist:
        return Response({"error": "Dispatch does not exist"}, status=status.HTTP_404_NOT_FOUND)

@swagger_auto_schema(method='delete')
@api_view(['DELETE'])
def delete_dispatch(request, dispatch_id):
    """
    Delete a dispatch.
    """
    try:
        dispatch = Dispatch.objects.get(dispatch_id=dispatch_id)
    except Dispatch.DoesNotExist:
        return Response(status=404)

    dispatch.delete()
    return Response(status=204)

def process_active_dispatches():
    current_time = datetime.now()
    active_dispatches = Dispatch.objects.filter(start_time__lte=current_time, end_time__gte=current_time)

    for dispatch in active_dispatches:
        clients = Client.objects.filter(dispatch.client_filter)
        for client in clients:
            last_message_id = Message.objects.aggregate(Max('message_id'))['message_id__max']
            new_message_id = last_message_id + 1

            message_data = {
                'id': new_message_id,
                'phone': client.phone_number,
                'text': dispatch.message_text
            }
            response = external_api.send_message_to_external_service(message_data)
            if response.status_code == 200:
                message = Message.objects.create(dispatch=dispatch, client=client, status='sent')
            else:
                # Handle error and retry sending later
                pass
