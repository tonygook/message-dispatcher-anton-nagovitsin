import requests

def send_message_to_external_service(message_data):
    url = 'https://probe.fbrq.cloud/v1/send/'+message_data[id]
    headers = {
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjA2MTAwNTUsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9ibGluZ3lfd2luZ2cifQ.ux3aDHXn_Nc3csSjNu8c3nLj3nehjwX40RfHWEWchmM',
        'Content-Type': 'application/json'
    }
    response = requests.post(url, json=message_data, headers=headers)
    return response
