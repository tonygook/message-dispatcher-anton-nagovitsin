from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from datetime import timedelta
from rest_framework.test import APIClient, APITestCase
from .models import Client, Dispatch, Message
from .serializers import ClientSerializer, DispatchSerializer, MessageSerializer
from rest_framework import status

class CreateClientTestCase(APITestCase):
    def test_create_client(self):
        url = '/clients/'
        data = {
            'id': 1,
            'phone_number': 1234567890,
            'mobile_operator_code': 'ABC',
            'tag': 'test',
            'timezone': 'UTC'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 1)
        self.assertEqual(int(Client.objects.get().phone_number), 1234567890)


class UpdateClientTestCase(APITestCase):
    def test_update_client(self):
        client = Client.objects.create(phone_number=1234567890, mobile_operator_code='ABC', tag='test', timezone='UTC')
        url = f'/clients/{client.pk}/'
        data = {
            'phone_number': 9876543210,
            'mobile_operator_code': 'DEF',
            'tag': 'updated',
            'timezone': 'CET'
        }

        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(str(Client.objects.get().phone_number), '9876543210')



class DeleteClientTestCase(APITestCase):
    def test_delete_client(self):
        client = Client.objects.create(phone_number=1234567890, mobile_operator_code='ABC', tag='test', timezone='UTC')
        url = f'/clients/{client.pk}/'

        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Client.objects.count(), 0)


class CreateDispatchTestCase(APITestCase):
    def test_create_dispatch(self):
        url = reverse('create_dispatch')
        data = {
            'start_time': timezone.now(),
            'end_time': timezone.now() + timedelta(hours=1),
            'message_text': 'Test message',
            # 'filter_property': 'operator_code',
            # 'filter_value': '123',
        }
        response = self.client.post(url, data, format='json')
        print(response.data)  # Print the response data
        print(response.status_code)  # Print the response status code
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class GetStatisticsTestCase(APITestCase):
    def test_get_statistics(self):
        url = reverse('get_statistics')
        response = self.client.get(url)
        print(response.data)  # Print the response data
        print(response.status_code)  # Print the response status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetDispatchStatisticsTestCase(APITestCase):
    def test_get_dispatch_statistics(self):
        dispatch = Dispatch.objects.create(start_time=timezone.now(), end_time=timezone.now() + timedelta(hours=1),
                                           message_text='Test message',)
                                        #    client_filter='operator_code', filter_value='123')
        url = reverse('get_dispatch_statistics', kwargs={'dispatch_id':dispatch.pk})
        response = self.client.get(url)
        print(response.data)  # Print the response data
        print(response.status_code)  # Print the response status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateDispatchTestCase(APITestCase):
    def test_update_dispatch(self):
        dispatch = Dispatch.objects.create(start_time='2022-01-01T10:00:00Z', end_time='2022-01-01T12:00:00Z', message_text='Test message')
        print(dispatch.pk)
        url = reverse('update_dispatch', kwargs={'dispatch_id':dispatch.pk})
        data = {
            'start_time': '2022-01-01T11:00:00Z',
            'end_time': '2022-01-01T13:00:00Z',
            'message_text': 'Updated message',
            # 'client_filter': {'mobile_operator_code': 'DEF', 'tag': 'updated'}
        }

        response = self.client.put(url, data, format='json')
        print(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Dispatch.objects.get().message_text, 'Updated message')


class DeleteDispatchTestCase(APITestCase):
    def test_delete_dispatch(self):
        dispatch = Dispatch.objects.create(start_time='2022-01-01T10:00:00Z', end_time='2022-01-01T12:00:00Z', message_text='Test message')
        url = f'/dispatches/{dispatch.pk}/'

        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Dispatch.objects.count(), 0)

