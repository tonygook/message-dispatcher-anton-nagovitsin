from django.db import models

class Client(models.Model):
    client_id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=12)
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=50)
    timezone = models.CharField(max_length=50)

class Dispatch(models.Model):
    dispatch_id = models.AutoField(primary_key=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    message_text = models.TextField()
    client_filter = models.Q(mobile_operator_code=models.F('client__mobile_operator_code'))\
        & models.Q(tag=models.F('client__tag'))

class Message(models.Model):
    message_id = models.AutoField(primary_key=True)
    created_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50)
    dispatch_id = models.ForeignKey(Dispatch, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
