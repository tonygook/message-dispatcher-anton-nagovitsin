from drf_yasg import openapi

client_schema = {
    'client_id': openapi.Schema(type=openapi.TYPE_INTEGER),
    'phone_number': openapi.Schema(type=openapi.TYPE_STRING),
    'mobile_operator_code': openapi.Schema(type=openapi.TYPE_STRING),
    'tag': openapi.Schema(type=openapi.TYPE_STRING),
    'timezone': openapi.Schema(type=openapi.TYPE_STRING),
}

dispatch_schema = {
    'dispatch_id': openapi.Schema(type=openapi.TYPE_INTEGER),
    'start_time': openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME),
    'end_time': openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME),
    'message': openapi.Schema(type=openapi.TYPE_STRING),
    'client_filter': openapi.Schema(type=openapi.TYPE_STRING),
}

message_schema = {
    'message_id': openapi.Schema(type=openapi.TYPE_INTEGER),
    'created_time': openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME),
    'status': openapi.Schema(type=openapi.TYPE_STRING),
    'dispatch_id': openapi.Schema(type=openapi.TYPE_INTEGER),
    'client_id': openapi.Schema(type=openapi.TYPE_INTEGER),
}

client_list_schema = openapi.Schema(
    type=openapi.TYPE_ARRAY,
    items=openapi.Schema(type=openapi.TYPE_OBJECT, properties=client_schema),
)
dispatch_list_schema = openapi.Schema(
    type=openapi.TYPE_ARRAY,
    items=openapi.Schema(type=openapi.TYPE_OBJECT, properties=dispatch_schema),
)
message_list_schema = openapi.Schema(
    type=openapi.TYPE_ARRAY,
    items=openapi.Schema(type=openapi.TYPE_OBJECT, properties=message_schema),
)

client_responses = {
    '200': 'Successful response',
    '400': 'Bad request',
    '404': 'Not found',
}

dispatch_responses = {
    '200': 'Successful response',
    '400': 'Bad request',
    '404': 'Not found',
}

message_responses = {
    '200': 'Successful response',
    '400': 'Bad request',
    '404': 'Not found',
}

client_list_response = {
    '200': 'Successful response',
}

dispatch_list_response = {
    '200': 'Successful response',
}

message_list_response = {
    '200': 'Successful response',
}

client_create_request = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties=client_schema,
    required=['phone_number', 'mobile_operator_code', 'tag', 'timezone'],
)

dispatch_create_request = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties=dispatch_schema,
    required=['start_time', 'end_time', 'message', 'client_filter'],
)

client_update_request = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties=client_schema,
)

dispatch_update_request = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties=dispatch_schema,
)

client_delete_response = {
    '204': 'Successful response',
    '404': 'Not found',
}

dispatch_delete_response = {
    '204': 'Successful response',
    '404': 'Not found',
}

client_create_responses = client_responses.copy()
client_create_responses.update({'201': 'Created'})

dispatch_create_responses = dispatch_responses.copy()
dispatch_create_responses.update({'201': 'Created'})

client_update_responses = client_responses

dispatch_update_responses = dispatch_responses

client_delete_responses = client_delete_response

dispatch_delete_responses = dispatch_delete_response

client_list = {
    'responses': client_list_response,
    'request_body': None,
}

dispatch_list = {
    'responses': dispatch_list_response,
    'request_body': None,
}

message_list = {
    'responses': message_list_response,
    'request_body': None,
}

client_create = {
    'request_body': client_create_request,
    'responses': client_create_responses,
}

dispatch_create = {
    'request_body': dispatch_create_request,
    'responses': dispatch_create_responses,
}

client_update = {
    'request_body': client_update_request,
    'responses': client_update_responses,
}

dispatch_update = {
    'request_body': dispatch_update_request,
    'responses': dispatch_update_responses,
}

client_delete = {
    'responses': client_delete_responses,
    'request_body': None,
}

dispatch_delete = {
    'responses': dispatch_delete_responses,
    'request_body': None,
}

openapi_config = {
    'clients': {
        'get': client_list,
        'post': client_create,
    },
    'clients/{client_id}': {
        'get': client_list,
        'put': client_update,
        'delete': client_delete,
    },
    'dispatches': {
        'get': dispatch_list,
        'post': dispatch_create,
    },
    'dispatches/{dispatch_id}': {
        'get': dispatch_list,
        'put': dispatch_update,
        'delete': dispatch_delete,
    },
    'messages': {
        'get': message_list,
    },
}



openapi_info = openapi.Info(
    title='Notification Service API',
    default_version='v1',
    description='API for managing notifications',
)

openapi_schema = {
    'openapi': '3.0.0',
    'info': openapi.Info(
        title='Notification Service API',
        default_version='v1',
        description='API for managing notifications',
    ),
    'paths': openapi_config,
}

openapi_dict = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties=openapi_schema,
)

