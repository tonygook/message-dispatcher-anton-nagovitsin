"""
URL configuration for notification_service project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from notification_app import views
from . import urls as notification_urls
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
# import os

# from rest_framework_swagger.views import get_swagger_view
from django.conf import settings
from django.conf.urls.static import static


router = routers.DefaultRouter()
router.register(r'clients', views.ClientViewSet)
router.register(r'dispatches', views.DispatchViewSet)
router.register(r'messages', views.MessageViewSet)


schema_view = get_schema_view(
    openapi.Info(
        title="Notification Service API",
        default_version='v1',
        description="API for managing notifications",
    ),
    public=True,
)


urlpatterns = [
    path("admin/", admin.site.urls),
    # path('api/', include(notification_urls)),
    path('', include(router.urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('openapi/', views.openapi_view, name='openapi'),
    path('create-client/', views.create_client, name='create-client'),
    path('update-client/<int:client_id>/', views.update_client, name='update_client'),
    path('delete-client/<int:client_id>/', views.delete_client, name='delete_client'),
    path('create-dispatch/', views.create_dispatch, name='create_dispatch'),
    path('statistics/', views.get_statistics, name='get_statistics'),
    path('get-dispatch-statistics/<int:dispatch_id>/', views.get_dispatch_statistics, name='get_dispatch_statistics'),
    # path('dispatches/<int:dispatch_id>/statistics/', views.get_dispatch_statistics, name='get_dispatch_statistics'),
    path('update-dispatch/<int:dispatch_id>/', views.update_dispatch, name='update_dispatch'),
    path('delete-dispatch/<int:dispatch_id>/', views.delete_dispatch, name='delete_dispatch'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
